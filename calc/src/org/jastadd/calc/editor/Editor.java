package org.jastadd.calc.editor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.jastadd.calc.editor.gen.Numeral;
import org.jastadd.calc.editor.gen.Program;
import org.jastadd.tree.edit.ASTNodeInterface;
import org.jastadd.tree.edit.CompilerContainer;
import org.jastadd.tree.edit.SubclassExtractor;
import org.jastadd.tree.edit.jtree.MainJtreeContatiner;
import org.jastadd.tree.edit.jtree.MainContainer;
import org.jastadd.tree.edit.custom.render.MainCustomContainer;
import org.jastadd.tree.edit.xml.JastAddXMLParser;
import org.jastadd.tree.edit.xml.JastAddXMLSerializer;

import internal.org.xmlpull.v1.XmlPullParserException;

public class Editor {

	private JFrame frame;
	private MainContainer mjc;
	
	public static Object DrAST_root_node;

	public Editor(byte[] data) throws IOException {
		frame = new JFrame("Calc");

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		mjc = new MainCustomContainer(data, new CompilerContainer(new SubclassExtractor("dir.txt")),
				"org.jastadd.calc.editor.gen");

		frame.getContentPane().add(mjc, BorderLayout.CENTER);
		frame.setJMenuBar(new MyMenu());

		frame.pack();

		frame.setVisible(true);
	}

	public void newEditorFromFile(File file) {
		frame.getContentPane().removeAll();
		RandomAccessFile f;
		byte[] data = null;
		try {
			f = new RandomAccessFile(file, "r");
			data = new byte[(int) f.length()];
			f.read(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		mjc = new MainCustomContainer(data, new CompilerContainer(new SubclassExtractor("dir.txt")),
				"org.jastadd.calc.editor.gen");
		frame.getContentPane().add(mjc, BorderLayout.CENTER);
		frame.revalidate();
	}

	public void saveEditorToFile(File file) {
			try {
				FileOutputStream f = new FileOutputStream(file);
				f.write(mjc.getData());
				f.flush();
				f.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

	}

	public static void main(String[] args) throws IOException {
		if(args.length == 0){
		    String root = JastAddXMLSerializer.serializ(new Program(new Numeral("0")));
			new Editor(root.getBytes());
		}else {
			RandomAccessFile f;
			byte[] data = null;
			try {
				f = new RandomAccessFile(args[0], "r");
				data = new byte[(int) f.length()];
				f.read(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
			new Editor(data);
		}
	}

	class MyMenu extends JMenuBar {
		final JFileChooser fc = new JFileChooser();

		public MyMenu() {
			JMenu menu = new JMenu("File");
			add(menu);
			JMenuItem jmi = new JMenuItem("Open");
			jmi.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int status = fc.showOpenDialog(jmi);
					if (status == JFileChooser.APPROVE_OPTION) {
						File f = fc.getSelectedFile();
						newEditorFromFile(f);
					}

				}
			});
			menu.add(jmi);
			JMenuItem jms = new JMenuItem("Save");
			jms.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int status = fc.showSaveDialog(jms);
					if (status == JFileChooser.APPROVE_OPTION) {
						File f = fc.getSelectedFile();
						saveEditorToFile(f);
					}

				}
			});
			menu.add(jms);
			JMenuItem jmq = new JMenuItem("Quit");
			jmq.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
                    System.exit(0);
				}
			});
			menu.add(jmq);
		}

	}

}
