Overview

    This directory contains information for running the artefact
    Jatte, reproducing the examples described in the paper "Jatte:
    A Tunable Tree Editor for Integrated DSLs."

System requirements:

    * Java 8

    * Ant

    * Unix shell

Intro Calc

    In this step by step instruction, we want to show how we created
    the  Calc based examples used in the paper.

    We begin with some orientation of the files used in the examples.
    The editor for Calc is inside the calc-folder. In this folder,
    in the file, the spec/ast/calc.ast is the abstract grammar for
    the Calc-language. This grammar is the same as Figure 1 in the
    paper. The file spec/rag/root.jrag specifies what node type act
    as the root. In the java file src/org/jastadd/calc/editor/Editor.java
    we have the code used for setting up the editor.The build.xml
    file contains the build script for the editor. Using the aspects
    files from Jatte and Calc, Jatte-lib and Editor.java, build.xml
    can build the editor in the form of a jar-file.

Default editor

    The first step of running the editor is to build it. From inside
    the folder calc using a terminal run:

	> ant

    Running "ant" weaves all the aspects and compile it into
    CalcEditor.jar. Ant also generates the file dir.txt. dir.txt
    is used by the editor for finding subclasses. This method for
    finding subclasses is a hack, and we are looking at better ways
    of solving the problem. Because of this hack, it is important
    that you always run the editor from the calc-folder.

    To start the editor and load the file mul.xml run:

	> java -jar CalcEditor.jar test-files/mul.xml

    Running this will open an editor looking like Figure 2 in the
    paper.  If you right-click on the "Expr:Mul" node you see a
    menu like the one in Figure 3, click on "Div" and see how the
    node-type change. To get the other menu in Figure 3 to pop-up,
    right-click on the "Left:Numeral" node. To modify the token of
    "Left:Numeral", edit the text field in the now visible menu and
    hit enter. You can also modify token of "Left:Numeral" by
    clicking on the token in the editor. The click will activate a
    text field for the token, hit enter for the change to get
    through.

Customize label

    In this part of the instructions, we want to show the code we
    used for customising the labels in the editor. Quit the editor
    and copy the file label.jrag into the spec/rag folder using the
    following command:

	> cp ../examples/label.jrag spec/rag

     Open the just copied file and note that it has some code. This
     code describes both what the label shall contain and what style
     to use. The code corresponds to the code in Figure 4 in the
     paper, but in the paper no styling is used. To build and open
     the new version of the editor, run:

	> ant

	> java -jar CalcEditor.jar test-files/mul.xml

    The newly open editor will look like Figure 5 in the paper.

Hide nodes

    To hide numeral nodes like in section 4.2 in the paper, create
    a file hide.jrag in spec/rag and put the following code in it:

	aspect hide {
	    eq Numeral.ed_show() = false;
	}

    Quit the editor and run:

       > ant

       > java -jar CalcEditor.jar test-files/mul.xml

    Now you have an editor similar to the one in Figure 6 in the
    paper. You can right-click on the "1*2", and the menu of Figure
    7 shows up. In the menu, you can edit the value of the hidden
    numerals. Try to change the "1" to a "3" by hovering over the
    "Left:Numeral" menu item and then change the "1" to a "3", in
    the now visible text field, and hit enter.

    The context-aware hiding described in section 4.3 is an extension
    of the "hide" aspect we just wrote. To make the editor use
    context-aware hiding, quit the current editor and run:

	> cp ../examples/hide.jrag spec/rag

	> ant

	> java -jar CalcEditor.jar test-files/let.xml

    The file "spec/rag/hide.jrag" should now contain code similar
    to the code in "Figure 8" in the paper. The editor now looks
    something like "Figure 9" in the paper.

Customizing menus

    The last example using Calc concerns customising menus and
    intelligent code completion. In this example, we customised the
    menu for setting the ID token of an IdUse. Instead of having a
    text-field we have a menu containing the variables in the scope.
    To add this feature quit the current editor and run:

	> cp ../examples/menu.jrag spec/rag

	> ant

	> java -jar CalcEditor.jar test-files/code-completion.xml

    The editor is now similar to the editor in Figure 11 in the
    paper. Right click on the last "c" in the program and hover
    over "Change id" to see the IdDecls in scope. You can now modify
    the ID by clicking on "a" or "b".

    If you are interested in how the customisation of menus and
    intelligent code completion is done, open the menu.jrag file
    in spec/rag. Here you see the tree following aspects:

	* possibleNames: contains attributes for calculating the
	names visible in the current scope.

	* menu: contains attributes that customise the menu.

	* CodeCom: contains a method used in defining the new type
	of menu item used in code completion.

Drag and Drop

	For those how are more interested, we also want to show our
	built-in support for using drag and drop to reorder lists.
	To include this feature open the build.xml file and locate
	the row looking like this:

	<!-- <include name="DnDList.jrag" /> -->

	Change this row to:

	<include name="DnDList.jrag" />

       To build and start the editor again, run the following
       commands:

	> ant

	> java -jar CalcEditor.jar test-files/code-completion.xml

	Try now to reorder the "a = 10" and "b = 5" by clicking and
	dragging "b = 5" over "a = 10" and see how they change
	place.
