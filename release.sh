#!/bin/bash
mkdir tmpGit
cd tmpGit
git clone -b newLable https://alfredakesson@bitbucket.org/jastadd/jatteartifactevaluation.git
cd jatteartifactevaluation
fmt to-edit.txt > README.txt
mkdir release
cp -r  jatte release/
cp README.txt release/
cp -r calc release/
cp -r examples release/
zip -r release.zip release
rm -r release
mv release.zip ../../releaseNewLook.zip
cd ../../
rm -r -f tmpGit
